import dateutil.parser
import datetime
from . import az_secrets


def _get_last_expiring_cred():
    """
    Get the latest cred, returning tuple of cred and error message
    """
    try:
        az_secrets.az_login()
        cred_list = az_secrets.get_current_creds()
    except az_secrets.AZNotFound:
        return [], "Azure CLI not found"
    except az_secrets.AZError as e:
        az_secrets.az_logout()
        return (
            [],
            "Failed to load secrets:\n {}".format(e.__cause__.stderr.decode("utf-8")),
        )
    az_secrets.az_logout()
    try:
        cred_list.sort(
            key=lambda x: (
                dateutil.parser.isoparse(x["endDateTime"]),
                dateutil.parser.isoparse(x["startDateTime"]),
            )
        )
        last_expiring_cred = cred_list.pop()
    except IndexError:
        return None, "No credentials found with prefix {prefix}"
    return last_expiring_cred, ""


def xymon_check_cred_age():
    """
    Return a tuple of a Xymon colour string and a message
    """
    colour = "red"
    cred, message = _get_last_expiring_cred()
    if cred:
        days_to_expiry = (
            dateutil.parser.isoparse(cred["endDateTime"])
            - datetime.datetime.now(datetime.timezone.utc)
        ).days
        if days_to_expiry < az_secrets.config["days_before_expiry_red"]:
            colour = "red"
        elif days_to_expiry < az_secrets.config["days_before_expiry_yellow"]:
            colour = "yellow"
        else:
            colour = "green"
        message = (
            "{days} days to expiry of newest credential whose name starts {prefix}"
        )
    return (
        colour,
        message.format(
            days=days_to_expiry, prefix=az_secrets.config["secret_name_prefix"]
        ),
    )


def xymon_compare_newest_cred(secret):
    """
    Compares its argument with the hint for the newest credential

    Returns tuple of Xymon colour string and message
    """
    colour = "red"
    cred, message = _get_last_expiring_cred()
    if cred:
        if cred["hint"] == secret[0:3]:
            colour = "green"
            message += "Newest secret hint matches secret in use"
        else:
            message += "Newest secret hint {hint} does not match secret in file"
    return (
        colour,
        message.format(
            prefix=az_secrets.config["secret_name_prefix"], hint=cred.get("hint")
        ),
    )
