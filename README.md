## This project has been retired

The replacement is at https://gitlab.developers.cam.ac.uk/ch/co/ucam-app-secrets
which aims to handle secrets for applications besides wsgetmail. It also
contains bugfixes.

## wsgetmail-secrets
Scripts for rotating secrets for the wsgetmail app which feeds email from ExOL to RT

## Description
This provides code to use the Azure CLI to rotate app secrets that wsgetmail uses to fetch mail and update its config files.

## Installation
This requires the Azure CLI to be installed. In Chemistry this would normally happen via Ansible applying the azure\_cli role.

To configure copy wsgetmail\_secrets/rotatecreds.yml to /etc/wsgetmail/rotatecreds.yml, or
for testing wsgetmail\_secrets/config.yml, and edit as needed.

## Usage

The scripts only operate on secrets whose name starts with the string set in
the configuration file. The app may have other secrets, which will be ignored.

rotate-rt-secrets will rotate secrets and print out the latest secret. It will
keep the new secret plus up to as many old ones as the config file says to.

template-wsgetmail-files looks in /etc/wsgetmail/templates for files ending
json.j2 . It rotates the credentials and then templates the new credential into
those files, writing them out as .json files in /etc/wsgetmail . In order that
Ansible can easily write the templates using the template modules, the template
marker used by template-wsgetmail-files for its own templating is '[[' and ']]'
not the standard '{{' and '}}'.
